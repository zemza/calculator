import { describe, it } from 'mocha';
import { expect } from 'chai';
import { Browser, Builder, By, Key, WebElement } from "selenium-webdriver";
const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds *100));

async function calculatorTest() {
  // launch the browser
  let driver = await new Builder().forBrowser("firefox").build();
  try {
    await driver.get("https://zemza.gitlab.io/calculator/");
    driver.sleep(20);
    //Find important elements
    const slider_Speed = await driver.findElement(By.id("volSpeed"));
    const slider_Angle = await driver.findElement(By.id("volAngle"));
    const slider_Distance = await driver.findElement(By.id("volDistance"));
    const value_S = await driver.findElement(By.id("valS"));
    const value_A = await driver.findElement(By.id("valA"));
    const value_d = await driver.findElement(By.id("valD"));

    //Check that results gets updated
    const Result = await driver.findElement(By.id("results")).getAttribute("value");
    const Button = await driver.findElement(By.id("nappula"))
    const String = (`Missed Goal by... 120.78 meters

    Horizontal Range: 160.78 meters
    
    Maximum Height: 33.73 meters
    
    Time of Flight: 5.25 seconds `);
    expect(Result).to.eq("Results...");
    await Button.click();
    expect(Result).to.eq(String);

    //test calculation
    Result = await driver.findElement(By.id("results")).getAttribute("value");
    Button = await driver.findElement(By.id("nappula"))
    String = (`GOOOALL!!! ball landed within 2.44 meter range between top bar of the goal post and the ground

    Horizontal Range: 51.34 meters
    
    Maximum Height: 9.32 meters
    
    Time of Flight: 2.76 seconds `);
    expect(Result).to.eq("Results...");
    await slider_Speed.sendKeys(Key.LEFT.repeat(17));
    await slider_ANgle.sendKeys(Key.LEFT.repeat(4));
    await slider_Distance.sendKeys(Key.RIGHT.repeat(11));
    await Button.click();
    expect(Result).to.eq(String);
} finally {
    await driver.quit();
  }
}
calculatorTest();